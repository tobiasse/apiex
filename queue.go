package main

type FIFO struct {
	jobs []Job
}

func (f *FIFO) Queue(job Job) {
	f.jobs = append(f.jobs, job)
}

func (f *FIFO) Dequeue() (Job, bool) {
	if f.Full() {
		job := f.jobs[0]
		f.jobs = f.jobs[1:]
		return job, true
	}
	return Job{}, false
}

func (f *FIFO) Full() bool {
	if len(f.jobs) > 0 {
		return true
	}
	return false
}
