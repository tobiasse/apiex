package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	flag "github.com/spf13/pflag"
	"os"
	"path"
)

var pathwaysFlag pathways
var outputsFlag outputs
var output string
var worker uint8
var ascsv bool

var jobs FIFO
var com = make(chan Job)
var runnning = uint8(0)

func main() {
	flag.ErrHelp = errors.New("apiex: help requested")
	flag.Usage = Usage

	cmd := flag.CommandLine
	cmd.VarP(&pathwaysFlag, "pathways", "p", "CSV file, outputs to fetch per pathway from the EUCalc API")
	cmd.VarP(&outputsFlag, "outputs", "o", "CSV file, pathways to fetch from the EUCalc API")
	cmd.StringVar(&output, "output", "", "Directory path, if set writes downloaded pathways to individual files named by ID column")
	cmd.Uint8VarP(&worker, "worker", "w", 3, "Number of parallel pathway downloads")
	cmd.BoolVarP(&ascsv, "csv", "c", false, "Get pathways as CSV instead of JSON")
	_ = cmd.Parse(os.Args)

	header := true

	if pathwaysFlag.path == "" || outputsFlag.path == "" {
		flag.Usage()
		return
	}

	for _, p := range pathwaysFlag.Get() {
		jobs.Queue(Job{
			Id:             p.Id,
			RequestPayload: EUCalcRequest{Levers: p, Outputs: outputsFlag.Get()},
		})
	}

	for jobs.Full() || runnning != 0 {
		for runnning < worker && jobs.Full() {
			if job, ok := jobs.Dequeue(); ok {
				go Worker(job, com)
				runnning++
			} else {
				fmt.Println("This should never happen")
				os.Exit(1)
			}
		}
		result := <-com
		runnning--
		if result.Error != nil {
			continue
		}
		if result.Success {
			if output != "" && !ascsv {
				text, _ := json.Marshal(result.ResponsePayload)
				_ = os.WriteFile(path.Join(output, fmt.Sprintf("%s.json", result.Id)), text, 0664)
			} else if output != "" && ascsv {
				_ = os.WriteFile(
					path.Join(output, fmt.Sprintf("%s.csv", result.Id)),
					[]byte(toCSV(result.Id, result.ResponsePayload, true)),
					0664,
				)
			} else if ascsv {
				fmt.Print(toCSV(result.Id, result.ResponsePayload, header))
				header = false
			} else {
				text, _ := json.Marshal(result.ResponsePayload)
				fmt.Printf("{\"id\":\"%s\",\"response\":%s}\n", result.Id, text)
			}
		} else {
			jobs.Queue(Job{
				Id:             result.Id,
				RequestPayload: result.RequestPayload,
			})
		}
	}
}

func toCSV(jobid string, response EUCalcResponse, header bool) string {
	var buffer bytes.Buffer
	format := "\"%s\",\"%s\",\"%s\",\"%s\",%d,%f\n"
	if header {
		buffer.WriteString("\"jobid\",\"id\",\"title\",\"country\",\"year\",\"value\"\n")
	}
	for _, output := range response.Outputs {
		for cntry, values := range output.Data {
			for i, value := range values {
				buffer.WriteString(
					fmt.Sprintf(format, jobid, output.Id, output.Title, cntry, output.Time[i], value),
				)
			}
		}
	}
	return buffer.String()
}

func Usage() {
	msg := `apiex - An EUCalc API data Exporter

APIEx is a batch downloader for the EUCalc API. As input it expects a list of pathways and outputs for batch
downloading. Both files should comply to the CSV format. Pathways should contain the following columns: ID a
unique name of the pathway configuration, COUNTRY the country to download, and L1 till L58 the lever configuration
of the pathway. ID is always interpreted as a string value, COUNTRY should be one of: AT, BE, BG, HR, CY, CZ, DK,
EE, FI, FR, DE, EL, HU, IE, IT, LV, LT, LU, MT, NL, PL, PT, RO, SK, SI, ES, SE, CH, UK, RW, WD, or EU, and L1 till L58
must be a numeric value between 1 and 4.

A pathways.csv example:
ID, COUNTRY, L1, ..., L58
foo, DE, 1, ..., 4
bar, IT, 1, ..., 4

Outputs contain a single column NAME that lists the outputs to download.

An outputs.csv example:
NAME
agr_crop-cons_food[kcal]
agr_emissions-CO2e[Mt]

The downloaded pathways are printed to STDOUT formatted as JSON string as shown below. The individual pathways are
separated by a '\n' and 'response' contains the data returned by the EUCalc API.
{"id":"foo","response":{}}`

	_, _ = fmt.Fprintf(os.Stderr, "%s\n\n", msg)
	_, _ = fmt.Fprintf(os.Stderr, "Options:\n")
	flag.PrintDefaults()
}
