package main

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
)

var EUCalc = "http://tool.european-calculator.eu/localised"

type EUCalcRequest struct {
	Levers  Levers  `json:"levers"`
	Outputs Outputs `json:"outputs"`
}

type EUCalcResponse struct {
	Outputs Outputs `json:"outputs"`
	Status  string  `json:"status"`
}

type Levers struct {
	Id         string               `json:"-"`
	Default    []float64            `json:"default"`
	Exceptions map[string][]float64 `json:"exceptions"`
}

type Outputs []Output

type Output struct {
	Id    string               `json:"id"`
	Title string               `json:"title,omitempty"`
	Time  []int                `json:"timeAxis,omitempty"`
	Data  map[string][]float64 `json:"data,omitempty"`
}

type Job struct {
	Id              string
	RequestPayload  EUCalcRequest
	ResponsePayload EUCalcResponse
	Success         bool
	Error           error
}

func Worker(job Job, com chan Job) {
	req, _ := json.Marshal(job.RequestPayload)
	buf := bytes.NewBuffer(req)
	res, err := http.Post(EUCalc, "application/json", buf)
	if err != nil {
		com <- Job{Error: err}
		return
	}
	payload, _ := io.ReadAll(res.Body)
	var obj EUCalcResponse
	_ = json.Unmarshal(payload, &obj)
	if obj.Status != "" {
		com <- Job{Id: job.Id, RequestPayload: job.RequestPayload}
	} else {
		com <- Job{Id: job.Id, ResponsePayload: obj, Success: true}
	}
}
