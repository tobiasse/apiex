package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
)

type outputs struct {
	path    string
	csv     *csv.Reader
	outputs Outputs
}

func (o *outputs) String() string {
	return o.path
}

func (o *outputs) Set(value string) error {
	o.path = value
	f, err := os.Open(value)
	if err != nil {
		return err
	}
	o.csv = csv.NewReader(f)
	row, err := o.csv.Read()
	if err != nil {
		return err
	}
	if len(row) != 1 || row[0] != "NAME" {
		return fmt.Errorf("malformatted")
	}
	return nil
}

func (o *outputs) Type() string {
	return "CSV"
}

func (o *outputs) Get() Outputs {
	if o.outputs != nil {
		return o.outputs
	}
	rows, err := o.csv.ReadAll()
	if err != nil {
		fmt.Printf("Unable to read outputs from %s", o.path)
		os.Exit(1)
	}
	for _, row := range rows {
		o.outputs = append(o.outputs, Output{Id: row[0]})
	}
	return o.outputs
}

type pathways struct {
	path   string
	csv    *csv.Reader
	levers []Levers
}

func (p *pathways) String() string {
	return p.path
}

func (p *pathways) Set(value string) error {
	p.path = value
	f, err := os.Open(value)
	if err != nil {
		return err
	}
	p.csv = csv.NewReader(f)
	row, err := p.csv.Read()
	if err != nil {
		return err
	}
	if len(row) != 60 {
		return fmt.Errorf("malformatted")
	}
	return nil
}

func (p *pathways) Type() string {
	return "CSV"
}

func (p *pathways) Get() []Levers {
	if p.levers != nil {
		return p.levers
	}
	rows, err := p.csv.ReadAll()
	if err != nil {
		fmt.Printf("Unable to read outputs from %s", p.path)
		os.Exit(1)
	}
	for _, row := range rows {
		id, country := row[0], row[1]
		var setting []float64
		for _, l := range row[2:] {
			if v, err := strconv.ParseFloat(l, 64); err == nil {
				setting = append(setting, v)
			} else {
				fmt.Println("Lever settings should be numeric")
				os.Exit(1)
			}
		}
		var levers Levers
		if country == "EU" {
			levers = Levers{Id: id, Default: setting}
		} else {
			levers = Levers{Id: id, Default: setting, Exceptions: map[string][]float64{country: setting}}
		}
		p.levers = append(p.levers, levers)
	}
	return p.levers
}
