# APIEx: An EUCalc API data Exporter
## Installation
On Windows please download the *apiex.exe* from the */build* directory and on Linux download the *apiex* executable.
You may execute apiex by calling it from the cmd in Windows or your shell on Linux.

## Usage

APIEx is a batch downloader for the EUCalc API. As input it expects a list of pathways and outputs for batch
downloading. Both files should comply to the CSV format. Pathways should contain the following columns: **ID** a
unique name of the pathway configuration, **COUNTRY** the country to download, and **L1** till **L58** the lever
configuration of the pathway. **ID** is always interpreted as a string value, **COUNTRY** should be one of: *AT, BE,
BG, HR, CY, CZ, DK,  EE, FI, FR, DE, EL, HU, IE, IT, LV, LT, LU, MT, NL, PL, PT, RO, SK, SI, ES, SE, CH, UK, RW, WD,
or EU*, and **L1** till **L58** must be a numeric value between *1* and *4*.

**A pathways.csv example:**

```
ID, COUNTRY, L1, ..., L58
foo, DE, 1, ..., 4
bar, IT, 1, ..., 4
```

Outputs contain a single column **NAME** that lists the outputs to download.

**A outputs.csv example:**

```
NAME
agr_crop-cons_food[kcal]
agr_emissions-CO2e[Mt]
```

The downloaded pathways are printed to STDOUT formatted as JSON string as shown below. The individual pathways are
separated by a `\n` and `response` contains the data returned by the EUCalc API.

```json
{"id":"foo","response":{}}
```

**Options:**

    --output string   Directory path, if set writes downloaded pathways to individual files named by ID column
    -o, --outputs CSV     CSV file, pathways to fetch from the EUCalc API
    -p, --pathways CSV    CSV file, outputs to fetch per pathway from the EUCalc API
    -w, --worker uint8    Number of parallel pathway downloads (default 3)


## Example

For Linux the script below shows how to split the downloaded pathways to individual files named by the value provided
in the **ID** column (optionally you may use the `--output` option).

```bash
#!/bin/bash

while read -r line; do 
  name=$(jq -r .id <<<"$line")
  jq .response <<<"$line" >"${name}.json"
done < <(apiex -o outputs.csv -p pathways.csv)
```